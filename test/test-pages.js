// mocha framework for running tests
// chai assertion library

var expect = require('chai').expect;
var request = require('request'); // HTTP client0
var apiURL = 'http://localhost:8080';

describe('status and content', () => {
  describe('Main page', () => {

    // status
    it('Main page content', (done) => {
      request(apiURL, (error, response, body) => {
        expect(body).to.equal('Hello World');
        done();
      });
    });

    // content
    it('Main page content', (done) => {
      request(apiURL, (error, response, body) => {
        expect(body).to.equal('Hello World');
        done();
      });
    });

  });

  describe('About page', () => {
    it('status', (done)=>{
      request(apiURL+'/about', (error, response, body)=> {
          expect(response.statusCode).to.equal(404);
          done();
      });
    });
  });
});





it('About page content', (done) => {
  request(apiURL+'/about', (error, response, body)  => {
    expect(response.statusCode).to.equal(404);
    done();
  });
});
